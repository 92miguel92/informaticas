//
//  SecundarioViewController.swift
//  Controllers
//
//  Created by Master Móviles on 10/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class SecundarioViewController: UIViewController {

    var nomFich: String = ""
    var path: String = ""
    @IBOutlet weak var text_lb: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        path = Bundle.main.path(forResource:self.nomFich,ofType:"txt")!
        text_lb.text = try? String(contentsOfFile:path,encoding:String.Encoding.utf8)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}

//
//  ViewController.swift
//  Controllers
//
//  Created by Master Móviles on 10/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func retornoDeSecundaria(segue: UIStoryboardSegue) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //obtenemos el controller destino y forzamos la conversión al tipo adecuado
        let controller = segue.destination as! SecundarioViewController
        //fijamosla propiedad "nomFich" al identificador del segue
        controller.nomFich = segue.identifier!
    }
}

